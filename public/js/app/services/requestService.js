(function (angular) {
    'use strict';

    var moduleName = 'requestService';

    angular.module(moduleName, [])
        .service(moduleName, commonService);

    commonService.$inject = ['$http', '$httpParamSerializerJQLike'];
    function commonService($http, $httpParamSerializerJQLike) {

        return {
            send: send,
            sendPromise: sendPromise
        };

        function send(method, url, data, successHandler) {

            var errorHandler = function (data, status) {
                console.error('Response error', status, data);
            };

            $http({
                method: method || 'GET',
                url: url,
                data: $httpParamSerializerJQLike(data || {}),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(successHandler, errorHandler);

        }

        function sendPromise(method, url, data) {
            return $http({
                method: method || 'GET',
                url: url,
                data: $httpParamSerializerJQLike(data || {}),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
        }

    }

})(window.angular);