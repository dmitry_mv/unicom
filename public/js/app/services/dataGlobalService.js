(function (angular) {
    'use strict';

    var moduleName = 'dataGlobalService';

    angular.module(moduleName, [])
        .service(moduleName, commonService);

    commonService.$inject = [];
    function commonService(){

        var vm = this;

        vm.products = [];

        return vm;

    }

})(window.angular);