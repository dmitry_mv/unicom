(function (angular) {
    'use strict';

    var moduleName = 'productsService';

    angular.module(moduleName, [])
        .service(moduleName, commonService);

    commonService.$inject = ['storageService'];
    function commonService(storageService) {

        var vm = this;

        vm.removeFromCart = function (product) {
            var products = storageService.storage.cart.products;
            for (var i = 0; i < products.length; i++) {
                if (products[i]['id'] == product.id) {
                    products.splice(i, 1);
                    product.in_cart = false;
                    storageService.save();
                    break;
                }
            }
        };

        vm.addToCart = function (product) {
            var products = storageService.storage.cart.products;
            if (!vm.existsById(products, product.id)) {
                products.push({
                    id: product.id,
                    name: product.name,
                    price: product.price,
                    count: 1
                });
                product.in_cart = true;
                storageService.save();
            }
        };

        vm.plusOne = function (product) {
            product.count++;
            storageService.save();
        };
        vm.minusOne = function (product) {
            if (product.count > 1) {
                product.count--;
                storageService.save();
            }
        };

        vm.existsById = function (arr, id) {
            for (var i = 0; i < arr.length; i++) {
                if (arr[i]['id'] == id) {
                    return true;
                }
            }
            return false;
        };

        vm.getProductsFromCart = function () {
            return storageService.storage.cart.products;
        };

        vm.removeAllFromCart = function () {
            storageService.storage.cart.products = [];
            storageService.save();
        };

        return vm;

    }

})(window.angular);