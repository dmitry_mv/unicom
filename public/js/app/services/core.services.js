(function (angular) {
    'use strict';

    angular.module('core.services', [
        'requestService',
        'configService',
        'dataGlobalService',
        'localStorageService',
        'storageService',
        'productsService'
    ]);

})(window.angular);
