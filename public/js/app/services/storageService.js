(function (angular) {
    'use strict';

    var moduleName = 'storageService';

    angular.module(moduleName, [])
        .service(moduleName, commonService);

    commonService.$inject = ['localStorageService', '$timeout'];
    function commonService(localStorageService, $timeout) {

        var vm = this;

        vm.storage = {};

        vm.setStorage = function (obj) {
            localStorageService.setObject('shop', obj);
        };

        vm.getStorage = function () {
            var shop = localStorageService.getObject('shop');
            if (Object.keys(shop).length > 0) {
                return shop;
            } else {
                var storage = {
                    is_sync: true,
                    cart: {
                        count: 0,
                        total: 0,
                        products: []
                    }
                };
                vm.setStorage(storage);
                return storage;
            }
        };

        vm.calculateCart = function () {

            var total = 0;
            var count = 0;
            vm.storage.cart.products.forEach(function (product) {
                count += product.count;
                total += product.price * product.count;
            });

            vm.storage.cart.total = total;
            vm.storage.cart.count = count;
        };

        vm.save = function () {
            vm.calculateCart();
            vm.sync();
            vm.setStorage(vm.storage);
        };

        vm.timer = null;
        vm.checkSync = function () {
            $timeout(function () {
                vm.sync();
            }, 5000);
        };

        vm.sync = function () {
            var is_online = navigator.onLine;
            if (is_online) {
                vm.sentSyncRequest(function (response) {
                    var data = response.data;
                    if (data.ans) {
                        vm.storage.is_sync = true;
                        vm.setStorage(vm.storage);
                    }
                });
            } else {
                vm.storage.is_sync = false;
                vm.checkSync();
            }
        };

        vm.sentSyncRequest = function (callback) {
            callback({
                code: 200,
                data: {
                    ans: true
                }
            });
        };

        vm.init = function () {
            vm.storage = vm.getStorage();
            vm.sync();
        };

        vm.init();

        return vm;

    }

})(window.angular);