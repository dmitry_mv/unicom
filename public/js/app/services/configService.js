(function (angular) {
    'use strict';

    var serviceName = 'configService';

    angular.module(serviceName, [])
        .factory(serviceName, service);

    service.$inject = ['config', 'requestService', 'dataGlobalService', '$q'];

    function service(config, requestService, dataGlobalService, $q) {

        var vm = this;
        vm.isLoaded = false;
        vm.deferred = {};
        vm.getData = function () {
            vm.deferred = $q.defer();
            requestService.send(
                "GET",
                config.baseUrl + config.path.models.products.read,
                {},
                function (response) {
                    var data = response.data;
                    dataGlobalService.products = data.products;
                    vm.isLoaded = true;
                    vm.deferred.resolve();
                }
            );
            return vm.deferred.promise;
        };

        vm.check = function () {
            if (vm.isLoaded) {
                return true;
            } else {
                return vm.getData();
            }
        };

        return vm;

    }

})(window.angular);