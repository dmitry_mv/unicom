(function (angular) {
    'use strict';

    var moduleName = 'productFactory';

    angular.module(moduleName, [])
        .factory(moduleName, factoryService);

    factoryService.$inject = ['dataGlobalService', 'storageService'];

    function factoryService(dataGlobalService, storageService) {

        // Define the constructor function.
        function Product(obj) {
            this.id = parseInt(obj.id) || null;
            this.name = obj.name || 'без названия';
            this.price = parseInt(obj.price) || 0;
            this.in_cart = checkProductIdInCart(this.id);
        }

        function checkProductIdInCart(product_id) {
            var products = storageService.storage.cart.products;
            for (var i = 0; i < products.length; i++) {
                if (products[i].id == product_id) return true;
            }
            return false;
        }

        return ( Product );

    }


})(window.angular);
