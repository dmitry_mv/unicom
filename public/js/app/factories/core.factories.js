(function (angular) {
    'use strict';

    angular.module('core.factories', [
        'productFactory'
    ]);

})(window.angular);
