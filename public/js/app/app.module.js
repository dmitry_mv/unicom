(function (angular) {
    'use strict';

    angular.module('mainApp', ['app.core']);

})(window.angular);