(function (angular) {
    'use strict';

    angular
        .module('mainApp')
        .config(config);

    config.$inject = ['$routeProvider', '$locationProvider'];
    function config($routeProvider, $locationProvider) {

        $routeProvider
            .when('/cart', {
                templateUrl: 'js/app/views/cart.html',
                controller: 'CartController',
                controllerAs: 'vm',
                resolve: {
                    checkConfiguration: checkConfiguration
                }
            })

            .when('/products', {
                templateUrl: 'js/app/views/products.html',
                controller: 'ProductsController',
                controllerAs: 'vm',
                resolve: {
                    checkConfiguration: checkConfiguration
                }
            });

        checkConfiguration.$inject = ['configService'];
        function checkConfiguration(configService) {
            return configService.check();
        }

        $routeProvider.otherwise({redirectTo: "/products"});

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    }

})(window.angular);

