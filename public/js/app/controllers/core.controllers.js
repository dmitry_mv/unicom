(function (angular) {
    'use strict';

    angular.module('core.controllers', [
        'ProductsController',
        'CartController',
        'HeaderController'
    ]);

})(window.angular);
