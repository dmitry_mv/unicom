(function (angular) {
    'use strict';

    var moduleName = 'ProductsController';

    angular.module(moduleName, [])
        .controller(moduleName, Controller);

    Controller.$inject = ['dataGlobalService', 'productsService', 'productFactory'];

    function Controller(dataGlobalService, productsService, productFactory) {

        var vm = this;

        vm.productsService = productsService;

        vm.products = [];
        vm.initList = function () {
            dataGlobalService.products.forEach(function (product) {
                vm.products.push(new productFactory(product));
            });
        };

        vm.init = function () {
            vm.initList();
        };

        vm.init();

    }

})(window.angular);