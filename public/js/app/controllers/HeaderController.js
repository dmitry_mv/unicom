(function (angular) {
    'use strict';

    var moduleName = 'HeaderController';

    angular.module(moduleName, [])
        .controller(moduleName, Controller);

    Controller.$inject = ['storageService'];

    function Controller(storageService) {

        var vm = this;
        vm.storageService = storageService;

    }

})(window.angular);