(function (angular) {
    'use strict';

    var moduleName = 'CartController';

    angular.module(moduleName, [])
        .controller(moduleName, Controller);

    Controller.$inject = ['dataGlobalService', 'productsService', 'productFactory'];

    function Controller(dataGlobalService, productsService, productFactory) {

        var vm = this;

        vm.productsService = productsService;

        vm.products = [];
        vm.initList = function () {
            vm.products = productsService.getProductsFromCart();
        };

        vm.init = function () {
            vm.initList();
        };

        vm.init();



    }

})(window.angular);