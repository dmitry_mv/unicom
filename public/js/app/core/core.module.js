(function (angular) {
    'use strict';

    angular.module('app.core', [
            'ngRoute',
            'core.factories',
            'core.services',
            'core.directives',
            'core.controllers',
            'core.filters'
        ]);

})(window.angular);
