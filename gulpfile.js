var config = {
    path: {
        app_super: 'public/js/app-super',
        app: 'public/js/app',
        bundle: 'public/js/bundle'
    },
    pug_groups: {
        other: {},
        directives: {}
    },
    pug: {
        pug_files: 'public/js/app/pug',
        html_files: 'public/js/app/views',

        html_files_custom: 'public/js/app/views/directives/sales',
        custom_tpl: 'directives/sales/*.pug'
    }
};
var global = {
    watch: false,
    emittyChangedFile: ''
};

var gulp = require('gulp'),
    ngHtml2Js = require("gulp-ng-html2js"),
    minifyHTML = require('gulp-minify-html'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    rev = require('gulp-rev'),
    plumber = require('gulp-plumber'),
    pug = require('gulp-pug'),
    notify = require('gulp-notify'),
    uglify = require('gulp-uglify'),
    gulpif = require('gulp-if');
    /*
    emitty = require('emitty').setup(config.pug.pug_files, 'pug', {
        makeVinylFile: true
    });
    */


gulp.task('scss-compress', function () {
    return gulp.src(config.path.app + '/scss/index.scss')
        .pipe(sass({style: 'expanded'}))
        //.pipe(autoprefixer('last 2 version'))
        //.pipe(gulp.dest('dest/styles/'))
        //.pipe(rename({suffix: '.min'}))
        .pipe(minifycss())
        .pipe(concat('app.css'))
        .pipe(rename({suffix: '.min'}))
        //.pipe(rev())
        .pipe(gulp.dest(config.path.bundle + '/css'))
});

gulp.task('js-compress', function () {
    return gulp.src(
        [
            config.path.app + '/app.module.js',
            config.path.app + '/app.routes.js',
            config.path.app + '/controllers/**/**/*.js',
            //'app/constants/*.js',
            config.path.app + '/services/**/**/*.js',
            //'app/filters/**/**/*.js',
            config.path.app + '/factories/**/**/*.js',
            config.path.app + '/filters/**/**/*.js',
            //'app/services/*.js',
            config.path.app + '/directives/**/**/*.js',
            config.path.app + '/core/core.module.js'

        ],
        {base: config.path.app + '/'}
        )
        .pipe(uglify())
        .pipe(concat('app.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(config.path.bundle + '/'));

});

gulp.task('pug', function () {

    return gulp.src([config.pug.pug_files + '/**/**/*.pug'])
        //return gulp.src([config.pug.pug_files + '/' + config.pug.custom_tpl])

        .pipe(plumber({
            errorHandler: notify.onError(function (error) {
                return error.message;
            })
        }))
        .pipe(pug({pretty: true}))
        .pipe(gulp.dest(config.pug.html_files));
});

//**************************************************


//for sass auto compile
gulp.task('dev', function () {

    global.watch = true;

    gulp.watch(config.pug.pug_files + '/**/**/*.pug', gulp.series('templates'))
        .on('all', function (event, filepath, stats) {
            global.emittyChangedFile = {
                path: filepath,
                stats: stats
            };
        });

    gulp.watch(config.path.app + '/scss/**/**/*.scss', gulp.series('scss-compress'));

    gulp.watch([
        config.path.app + '/app.module.js',
        config.path.app + '/app.routes.js',
        config.path.app + '/core/core.module.js',
        //config.path.app + '/constants/*.js',
        config.path.app + '/controllers/**/**/*.js',
        config.path.app + '/directives/**/**/*.js',
        config.path.app + '/services/**/**/*.js',
        config.path.app + '/factories/**/**/*.js',
        config.path.app + '/filters/**/**/*.js'
    ], gulp.series('js-compress'));

});

function getPath() {
    return global.emittyChangedFile.path;
}
function getPathForSave() {
    var arr = global.emittyChangedFile.path.split('\\');
    var str = '';
    for (var i = 0; i < arr.length - 1; i++) {
        str += arr[i] + '\\';
    }
    return str.replace('pug\\', 'views\\');
}
gulp.task('templates', function () {
    return gulp.src(getPath())
        .pipe(plumber({
            errorHandler: notify.onError(function (error) {
                return error.message;
            })
        }))
        .pipe(pug({pretty: true}))
        .pipe(gulp.dest(getPathForSave()));
});

